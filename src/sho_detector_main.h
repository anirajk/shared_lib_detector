

#define PE_ARGP_APP_FILE    0x474747
#define PE_ARGP_REC_FILE    0x474748
#define PE_ARGP_WAIT_TIME   0x474749
#define PE_ARGP_DUMP_TIMING 0x47474a
#define PE_ARGP_DUMP_DEBUG  0x47474b
#define PE_ARGP_DISABLE_RECOVERY 0x47474c
#define PE_ARGP_A3_SERVER   0x47474d

struct pe_argp_state 
{
    char    *app_file_path;
    char    *recovery_rules_file;
    int	    wait_time;
    int	    dump_timing;
    int	    dump_debug;
    int	    disable_recovery;
#ifdef ENABLE_A3
    char    *a3_server;
#endif

    int	    argc;
    char    **argv;
    /* Grab this from the child parser. */
    struct target_spec *tspec;
};


error_t
pe_argp_parse_opt(int key,char *arg,struct argp_state *state);

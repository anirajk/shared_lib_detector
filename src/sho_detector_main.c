#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include <inttypes.h>
#include <signal.h>
#include <argp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "config.h"
#include "log.h"
#include "dwdebug.h"
#include "target_api.h"
#include "target.h"
#include "probe_api.h"
#include "probe.h"
#include "alist.h"
#include "list.h"

#include "sho_detector_utils.h"
#include "sho_detector_main.h"

#ifdef HAVE_CLIPSSRC
#include "clips.h"
#else
#include <clips/clips.h>
#endif

/* All Global variables */
struct		pe_argp_state	opts;
struct		target		*target = NULL;
unsigned char	*res = NULL;
char		**sys_call_names = NULL;
unsigned long	*sys_call_table  = NULL;
unsigned long   **function_prologue = NULL;
char 		base_fact_file[100];
ADDR 		syscall_table_vm;

struct argp_option pe_argp_opts[] = {
    { "pe-app-knowledge-file",PE_ARGP_APP_FILE,"FILE",0,"The application knowledge CLIPS file; defaults to ./application_knowledge.cls.",0 },
    { "pe-recovery-rules-file",PE_ARGP_REC_FILE,"FILE",0,"The recovery rules CLIPS file; defaults to ./recovery_constructs.cls",0 },
    { "pe-interval",PE_ARGP_WAIT_TIME,"SECONDS",0,"The wait time in between policy engine checks of the domain; defaults to 120 seconds",0 },
    { "pe-dump-timing",PE_ARGP_DUMP_TIMING,NULL,0,"Dump timing info to stderr.",0 },
    { "pe-dump-debug",PE_ARGP_DUMP_DEBUG,NULL,0,"Dump debug info to stdout.",0},
    { "pe-disable-recovery",PE_ARGP_DISABLE_RECOVERY,NULL,0,"Disable the recovery component.",0},
#ifdef ENABLE_A3
    { "pe-a3-server",PE_ARGP_A3_SERVER,"IP:PORT",0,"Report A3 events to the indicated server",0},
#endif
        { 0,0,0,0,0,0 },
};

struct argp pe_argp = {
    pe_argp_opts,pe_argp_parse_opt,NULL,NULL,NULL,NULL,NULL, };


error_t 
pe_argp_parse_opt(int key,char *arg,struct argp_state *state) 
{
    struct pe_argp_state *opts = \
        (struct pe_argp_state *)target_argp_driver_state(state);

    switch (key) 
    {
    
	case ARGP_KEY_ARG:
        return ARGP_ERR_UNKNOWN;
    
	case ARGP_KEY_ARGS:
        /* Eat all the remaining args. */
	if (state->quoted > 0)
            opts->argc = state->quoted - state->next;
        else
            opts->argc = state->argc - state->next;
        if (opts->argc > 0) {
            opts->argv = calloc(opts->argc,sizeof(char *));
            memcpy(opts->argv,&state->argv[state->next],opts->argc*sizeof(char *));
            state->next += opts->argc;
        }
        return 0;
    
	case ARGP_KEY_INIT:
        target_driver_argp_init_children(state);
        return 0;
    
	case ARGP_KEY_END:
	case ARGP_KEY_NO_ARGS:
	case ARGP_KEY_SUCCESS:
	    opts->tspec = target_argp_target_spec(state);
	    return 0;
    
	case ARGP_KEY_ERROR:
	case ARGP_KEY_FINI:
	    return 0;

	case PE_ARGP_APP_FILE:
	    opts->app_file_path = arg;
	    break;
    
	case PE_ARGP_REC_FILE:
	    opts->recovery_rules_file = arg;
	    break;
    
	case PE_ARGP_WAIT_TIME:
	    opts->wait_time = atoi(arg);
	    break;
    
	case PE_ARGP_DUMP_TIMING:
	    opts->dump_timing = 1;
	    break;
    
	case PE_ARGP_DUMP_DEBUG:
	    opts->dump_debug = 1;
	    break;
    
	case PE_ARGP_DISABLE_RECOVERY:
	    opts->disable_recovery = 1;
	    break;

	default:
	    return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

target_status_t cleanup() 
{
    target_status_t retval;
    target_pause(target);
    retval = target_close(target);
    target_finalize(target);
    return retval;
}

void 
sigh(int signo) 
{
    if (target) {
        target_pause(target);
        fprintf(stderr,"Ending monitoring on signal %d.\n",signo);
        cleanup();
        fprintf(stderr,"Ended monitoring.\n");
    }
    exit(0);
}

int
get_shared_objects(void)
{
    int			    result = 0;
    target_status_t	    status;
    static struct timeval   tm1;
    static struct timeval   tm2;
    unsigned long long	    t;

    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);
    
    /* Pause the target */
    if ((status = target_status(target)) != TSTATUS_PAUSED) {
        if (target_pause(target)) {
                fprintf(stderr,"ERROR: Failed to pause the target \n");
                result = 1;
                goto resume;
         }
    }

    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to pause the target is %llu ms\n", t);
    }
    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);

    result = object_info(target);
    if(result)
    {
	fprintf(stderr,"ERROR: object_info failed.\n");
	result  = 1;
	goto resume;
    }
    
    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to get file info is %llu ms\n", t);
    }

resume:
    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);
    if ((status = target_status(target)) == TSTATUS_PAUSED) {
        if (target_resume(target)) {
            
        }
    }
    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to resume the target is %llu ms\n", t);
    }

    return result;
}

void
register_signal_handlers(void)
{
    signal(SIGHUP,sigh);
    signal(SIGINT,sigh);
    signal(SIGQUIT,sigh);
    signal(SIGABRT,sigh);
    signal(SIGSEGV,sigh);
    signal(SIGPIPE,sigh);
    signal(SIGALRM,sigh);
    signal(SIGTERM,sigh);
    signal(SIGUSR1,sigh);
    signal(SIGUSR2,sigh);
}


int
main(int argc, char** argv)
{
    int			result	    =   0;
    int			iteration   =	0;
    struct target_spec	*tspec	    =   NULL;
    char		targetstr[80];
    target_status_t	tstat;

    memset(&opts,0,sizeof(opts));
    tspec = target_argp_driver_parse_one(&pe_argp,&opts,argc,argv,
					 TARGET_TYPE_XEN | TARGET_TYPE_GDB,1);
    if (!tspec)
    {
	fprintf(stderr,"ERROR: Could not parse target arguments!\n");
	exit(-1);
    }

    register_signal_handlers();
    dwdebug_init();
    target_init();
    atexit(target_fini);
    atexit(dwdebug_fini);

    target = target_instantiate(tspec,NULL);
    if (!target)
    {
	fprintf(stderr,"ERROR: Could not instantiate target!\n");
	exit(0);
    }

    if (target_open(target))
    {
	fprintf(stderr,"ERROR: Could not open %s!\n",targetstr);
	exit(0);
    }

    InitializeEnvironment();

    result = save_sys_call_table_entries(target, opts.dump_debug, syscall_table_vm);
    if(result)
    {
	fprintf(stderr,"ERROR: Failed to save the initial system call table entries.\n");
	exit(0);
    }

    while(1)
    {
	static struct timeval	tm1;
	static struct timeval	tm2;
	int			retries;

	fprintf(stdout,"============================ ITERATION %d ============================\n",iteration++);
	for (retries = 0; retries < 10; retries++) 
	{
	    struct timespec ts;

	    printf("Retrieving shared objects info...\n");
	    gettimeofday(&tm1, NULL);
	    result = get_shared_objects();
	    gettimeofday(&tm2, NULL);

	    if (result == 0)
		break;
	    /* sleep long enough to let the other domain run */
	    fprintf(stdout, "INFO: snapshot generation failed, trying again\n");
	    ts.tv_sec = 0;
	    ts.tv_nsec = (1000000000 / 10);
	    nanosleep(&ts, NULL);
	}

	if(iteration >= 2)	
	    break;

	timersub(&tm2, &tm1, &tm2);
	unsigned long long t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
	fprintf(stdout," Sleeping for %d seconds before the next iteration.\n", opts.wait_time);
	sleep(opts.wait_time);

    }
    fflush(stderr);
    fflush(stdout);
    tstat = cleanup();
    if(tstat == TSTATUS_DONE) {
        fprintf(stdout, " Monitoring finished.\n");
        return 0;
    }
    else if (tstat == TSTATUS_ERROR) {
        fprintf(stdout, "Monitoring failed!\n");
        return 1;
    }
    else {
        fprintf(stdout, "Monitoring failed with %d!\n",tstat);
        return 1;
    }

    return 0;
}

